let grades = [
   
98.5, 94.3, 89.2, 90.1];

console.log(grades[0]);

// An object is similar to an array. It is a collection of related data and /or functionalities 
// Usually it represents real world objects.

let grade = {
	// object initializer/literal notation - the Objects consist of properties, which are used to describe an object
	// Key-Value pair.
    math: 89.2,
    english: 98.5,
    science: 90.1,
    filipino: 94.3
}
// we use dot notation to access the properties/value of our object.
console.log(grade.english);

//Syntax
/*
let obejctName = {
	KeyA: valueA,
	KeyB: valueB,
}
*/

let cellphone = {
	brandName : 'Nokia 3310',
	color: 'Dark Blue',
	manufacturerDate: 1999
}
console.log(typeof cellphone)

let student = {
	firstName: 'John',
	lastName: 'Smith',
	mobileNo: 0912345678910,
    location: {
    	city: 'Tokyo',
    	country: 'Japan'
    },
    emails: ['john@gmail.com', 'johnsmith@gmail.com'],
   // Object method(function inside an object)
    fullName: function(){
    	return this.firstName + '' + this.lastName
    }

}

//dot notation
console.log(student.location.city);
//bracket notation
console.log(student['firstName'])
console.log(student.emails)
console.log(student.emails[0])
console.log(student.fullName())
// sample for this. 
const person1 = {
	name: 'Jane',
	greeting: function(){
		return 'Hi I\'m ' + this.name
	}
}
console.log(person1.greeting())

// ARRAY OF OBJECTS 
let contactList = [{
      firstName: 'John',
      lastName: 'Smith',
      location: 'Japan'
},
{
      firstName: 'Jane',
      lastName: 'Smith',
      location: 'Japan'
},
{
      firstName: 'Jasmine',
      lastName: 'Smith',
      location: 'Japan'
}
]
console.log(contactList[0].firstName)




let people = [

{
	name: 'Juanita',
	age: 13
},
{
	name: 'Juanito',
	age: 14
}

]
people.forEach(function(person){
	console.log(person.name)
})
console.log(' ${people[0].name} are the list')
//Creating objects using a Constructor Functions(JS OBJECTS CONSTRUCTORS/OBJECT FROM BLUEPRINTS)

//Creates reusable fuction to create several objects that have the same data structure 
//This is useful for creating multiple ccopies/instances of an object

//Object Literals 
//let object = {}
//Instances - distinct/ unique objects
//let object = new object 
// An instance is a concrete occurence of any object which emphasizes on the unique identity of it

/*
Snyntax:
function ObjectName(keyA, keyB){
	this.keyA = keyA,
	this.keyB= keyB
}
*/
function Laptop(name, manufacturerDate)
 {  //this keyword allows to assign a new object's property by associating them with values received from our parameter 
	this.name = name,
	this.manufacturerDate = manufacturerDate
}

//This is a unique instance of the Laptop object.

let laptop = new Laptop('Lenovo', 2008)
console.log('Result from creating objects using object constructors')
console.log(laptop);

//This is another unique instance of the Laptop object
let myLaptop = new Laptop('MacBook', 2020)
console.log('Result from creating objects using object constructors')
console.log(myLaptop);


//Creating empty objects 
let computer = {}
let myComputer = new Object();

console.log(myLaptop.name)
console.log(myLaptop['name'])

let array = [laptop, myLaptop]

console.log(array[0].name)


//Intializing/Adding/Deleting/Reassigning Object Properties 

//Initialized/added properties after the Object was created
//This is useful for items when an object's properties are undetermined at the time of creating ihem

let car = {}

//add an object properties, we can use dot notation

car.name = 'Honda Civic'
console.log(car); 
car['manufacture Date'] = 2019;
console.log(car);
car.name = 'Honda Civic';
console.log(car);

//Deleting object properties 
delete 	car['manufacture Date']
console.log(car);


//OBJECT METHODS
// A METHOD IS A FUNCTION WHICH IS A PROPERTY OF AN OBJECT
// THEY ARE ALSO FUNCTIONS AND ONE OF THE KEY DIFFERENCE THEY HAVE IS THAT METHODS ARE FUNCTIONS RELATED TO A SPECIFIC OBJECT 

let person ={
	name: 'John',
	talk: function(){
		console.log('Hello my name is ' + this.name)
	}
}

console.log(person);
person.talk();

// Adding methods to object person
person.walk = function(){
	console.log(this.name  + ' walked 25 steps forward')
}
person.walk();
let friend = {
	firstName: 'Joe',
	lastName: 'Smith',
	address:{
		city: 'Austria',
		country: 'Texas'
	};
	emails:['joes', 'joesmith'],
	introduce:function(){
		console.log('Hello my name is ' + this.firstName + '' + this.lastName)
	}
friend.introduce()


let myPokemon = {
	name: 'Pikachu',
	level: 3,
	health: 100,
	attack: 50,
	tackle: function(){
		console.log('This pokemon tackled the target Pokemon')
		console.log("'tagetPokemon's' health is now reduced to")
	}
	faint: function(){
		console.log('Pokemon fainted')
		
	}
}

function Pokemon(name,level){
	this.name= name;
	this.level= level;
	this.health = 2 * level;
	this.attack = level;

//Methods
this.tackle = function(target){
	console.log(this.name + ' tackled ' + target.name)
	console.log("targetPokemons's health is now reduced to targetPokemonHealth")

};

this.faint = function(){
	console.log(this.name + ' fainted.')
}

}


let pikachu = new Pokemon('Pikachu',16)
let charizard = new Pokemon('Charizard' 8)

